# Social Network

Création d'un messagerie en JavaScript (POO) façon MVC

## Objectifs

* Créer une messagerie en POO
* Séparer le code en Modèle Vue Controleur (MVC)
* Manipuler le DOM avec jQuery
