
// Déclaration variables globales
var userData = [], currTheme;
var currUserOnline = "0";

// Quand le DOM est ready, on initialise nos variables et on lance la fonction init()
$(function() {

    init();

    // Menu déroulant theme
    $(document).on('change','#themes-list', { passive: true }, function() {
        changeTheme();
    });

    // Quand on click sur le bouton du formulaire #add-user
    $( "#add-user" ).submit(function( event ) {
        let mail = $('#email');
        let age = $('#age');
        //preventDefault() evite le rechargement de la page
        event.preventDefault();

        let user;
        let intAge = parseInt(age.val());

        // Si intAge est plus grand ou égale à 16 && plus petit ou égale à 120 alors:
        // On initialise un nouvelle objet User dans la variable user et on lui passe la valeur de mail (récupéré de l'input) comme paramètre,
        // On lui attribut une nouvelle valeur age grâce à la methode de l'objet setAge() -> voir class User dans js/models/user.js
        // On fait de même pour le nom
        // Enfin on ajoute la variable user au tableau userData (méthode push())
        if (intAge >= 16 && intAge <= 120) {
            user = new User(mail.val());
            user.setAge(intAge);
            user.setUserName($('#user-name').val());
            userData.push(user);
            addToUserList();
        }


        // Pour chaque balise input contenus dans le formulaire #add-user
        // on réinitialise la valeur par rien! Cela permet de supprimer les informations rentrées au préalable
        $('#add-user input').each(function() {
            $(this).val("");
        });

    });

    // Au click sur button #profil crée user-displayer
    $(document).on('click','#profil', { passive: true }, function() {
        let index = $('body').find('#card-list option:selected').val();
        let intIndex = parseInt(index);
        let user = userData[intIndex];
        new UserDisplayer(user);
    } )

    // A click sur .close supprime le parent (.profil-container)
    $(document).on('click','.card-close', { passive: true }, function() {
        $(this).parent().remove()
    })

    // Quand on click sur le bouton du formulaire #add-post
    $( "#add-post" ).submit(function( event ) {
        event.preventDefault();
        //preventDefault() evite le rechargement de la page
        let title = $('body').find('#add-post #title').val();
        let text = $('body').find('#add-post #text').val();
        let post = new Post(title, text);
        let index = $('body').find('#user-list option:selected').val();
        let intIndex = parseInt(index);
        let user = userData[intIndex];
        user.addPost(post);

        new PostDisplayer(user, post);
    });
});

function init() {
    // Insert le menu déroulant themes
    $('<div id="themes"><span>Themes: </span><select id="themes-list"><option value="4">Purple</option><option value="1">theme 1</option></select></div>').insertBefore('h1');

    // Ajout des inputs, bouttons et conteneur liste déroulante pour les posts
    $( "#add-post" ).html('<select id="user-list"></select><input type="title" name="title-name" id="title" placeholder="Titre" /><label for="text"></label><textarea id="text" name="text"></textarea><button type="submit">Ajouter</button>');


    // Insert div pour le display user et post
    $("<div id='user-display'> </div>").insertAfter('#add-user');
    $("<div id='post-display'> </div>").insertAfter('#add-post');

    // InsertAfter menu déroulant card-list
    $('<div id="user-card"><select id="card-list"></select><button id="profil" type="button">Voir le profil</button></div>').insertAfter('#add-user');

    // Ajout des objets User + Post dans le tableau userData
    loadUserInData();

    // Change le theme
    changeTheme();


}


// Boucle le tableau tempData et créer un nouvelle objet User
// Passe les paramètres contenus dans le tableau (email, nom, age)
// Ajoute l'objet dans le tableau userData
function loadUserInData() {
    let tempData=[
        ["nadia@gmail.com", "Nadia", 20, "Bonjour!"],
        ["ilhem@gmail.com", "Ilhem", 20, "Salut!"],
        ["mubarak@gmail.com", "Mubarak", 20, "Hello!"],
        ["lionel@gmail.com", "Lionel", 20, "Yo!"],
    ]
    let userToAdd, postToAdd;
    let text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    for (var t = 0; t < tempData.length; t++) {
        for (var i = 0; i < tempData[t].length; i++) {
            switch (i) {
                case 1:
                    userToAdd.setUserName(tempData[t][i]);
                    break;
                case 2:
                    userToAdd.setAge(tempData[t][i]);
                    break;
                case 3:
                    postToAdd = new Post(tempData[t][i], text);
                    userToAdd.addPost(postToAdd);
                    new PostDisplayer(userToAdd, postToAdd);
                    break;
                default:
                    userToAdd = new User(tempData[t][i]);
                    userToAdd.value = t;
            }
        }
        userData.push(userToAdd);
    }
    addToUserList();
}

// Inject des balises options en fonction du nombre de User dans userData
function addToUserList() {
    let menus = ['#user-list', '#card-list'];

    for (var i = 0; i < menus.length; i++) {
        var sel = $('body').find(menus[i]);
        let nbOption = $('body').find(menus[i]+' option').length;

        $(userData).each(function(index) {
            if (nbOption == 0 || index >= nbOption) {
                $('<option>').appendTo(sel).attr('value',index).text(this.getUserName());
                this.value = index;
            }
        });
    }
}

function changeTheme() {
    let value = $('body').find('#themes-list option:selected').val();
    $('body').attr('class', 'style'+value);
    if (value == "4") {
        changeToStyle4();
    }
    currTheme = value;
    if (currTheme != "4") {
        removeFromStyle4();
    }
}
