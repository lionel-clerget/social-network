
/**
 * ICI est codée la vue de Post
 * Cette classe sert à afficher des données, PAS à les manipuler !!!
 */

/**
 * La classe post-displayer permet d'afficher un Post, et UNIQUEMENT à l'afficher
 */
class PostDisplayer {
    constructor(user, post) {
        this.post = post;
        this.user = user;
        this.userPost();

    }

    userPost(){
        let container = $('body').find('#post-display');
        
        $(`<div class="post-container"><div class="post-profil"><p class="post-nom"><span>Nom: </span>${this.user.getUserName()}</p><i class="avatar fas fa-user-circle" style="color:${this.user.getIconColor()}"></i><div class="option-container" data-value="${this.user.value}"></div></div><div class="post-content"><div class="post-title"><p><span>Titre: </span>${this.post.getTitle()}</p></div><div class="post-text"><p><span>Post: </span>${this.post.getText()}</p></div></div></div>`).appendTo(container);

    }

}
