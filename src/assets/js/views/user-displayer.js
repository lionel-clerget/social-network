
/**
 * ICI est codée la vue de User
 * Cette classe sert à afficher des données, PAS à les manipuler !!!
 */

/**
 * La classe user-displayer permet d'afficher un User, et UNIQUEMENT à l'afficher
 */
class UserDisplayer {
    constructor(user) {
        this.user = user;
        this.userCard();

    }

    userCard(){
        let container = $('body').find('#user-display');
        $(`<div class="card-container">
            <i class="fas fa-user-circle" style="color:${this.user.getIconColor()}"></i>
            <a class="card-close" name="button"><i class="far fa-times-circle"></i></a>
            <div class="card-infos">
                <p class="card-nom"><span>Nom:</span> ${this.user.getUserName()}</p>
                <p class="card-mail"><span>Email:</span> ${this.user.getEmail()}</p>
                <p class="card-age"><span>Age:</span> ${this.user.getAge()}</p>
            </div>
        </div>`).appendTo(container);

    }
}
